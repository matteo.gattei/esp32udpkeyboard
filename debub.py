import socket

UDP_IP = "192.168.2.27"
UDP_PORT = 10002

def send_udp_message(message):
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.sendto(message.encode(), (UDP_IP, UDP_PORT))

if __name__ == "__main__":
    message = input("Enter the message to send: ")
    send_udp_message(message)
    print(f"Message '{message}' sent to {UDP_IP}:{UDP_PORT}")