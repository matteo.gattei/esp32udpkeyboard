#ifndef WEBSERVER_HANDLER_H
#define WEBSERVER_HANDLER_H

#include <ESPAsyncWebServer.h>
#include "StorageHandler.h"

class WebServerHandler {
  public:
    WebServerHandler();
    void begin(StorageHandler* storageHandler);
    void handleClient();

  private:
    AsyncWebServer server;
    StorageHandler* storageHandler;
};

#endif
