#ifndef TIME_HANDLER_H
#define TIME_HANDLER_H

#include <NTPClient.h>
#include <WiFiUdp.h>
#include <Timezone.h>

class TimeHandler {
  public:
    TimeHandler();
    void begin();
    String getFormattedTime();

  private:
    WiFiUDP ntpUDP;
    NTPClient timeClient;
    Timezone italyTZ;
    void updateTime();
};

#endif
