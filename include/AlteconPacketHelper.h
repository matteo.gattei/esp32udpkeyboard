#ifndef ALTECON_PACKETHELPER_H
#define ALTECON_PACKETHELPER_H

#include <Arduino.h>
#include "KeyboardHandler.h"

class AlteconPacketHelper {
public:
    static void handlePacket(const char* packet, KeyboardHandler &keyboardHandler);
};

#endif // ALTECON_PACKETHELPER_H