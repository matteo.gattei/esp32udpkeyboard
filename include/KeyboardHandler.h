#ifndef KEYBOARD_HANDLER_H
#define KEYBOARD_HANDLER_H

#include <USBHIDKeyboard.h>

class KeyboardHandler {
  public:
    void begin();
    void typeText(const char* text);

  private:
    USBHIDKeyboard Keyboard;
};

#endif
