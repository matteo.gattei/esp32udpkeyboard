#ifndef UDP_HANDLER_H
#define UDP_HANDLER_H

#include <WiFiUdp.h>
#include "KeyboardHandler.h"
#include "StorageHandler.h"
#include "TimeHandler.h"

class UDPHandler {
  public:
    void begin(StorageHandler* storageHandler, TimeHandler* timeHandler);
    void handlePacket(KeyboardHandler &keyboardHandler);

  private:
    WiFiUDP Udp;
    StorageHandler* storageHandler;
    TimeHandler* timeHandler;
    const unsigned int localUdpPort = 10002;
    char incomingPacket[255];
};

#endif
