#ifndef STORAGE_HANDLER_H
#define STORAGE_HANDLER_H

#include <Arduino.h>
#include <FS.h>
#include <SPIFFS.h>

class StorageHandler {
  public:
    void begin();
    void saveCommand(const String &command, const String &timestamp);
    String getLatestCommands();

  private:
    const char* filePath = "/latest_commands.txt";
    void checkFileSize();
};

#endif
