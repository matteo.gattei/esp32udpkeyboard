#ifndef WIFI_HANDLER_H
#define WIFI_HANDLER_H

#include <WiFi.h>

class WiFiHandler {
  public:
    void begin();
    String getLocalIP();
    String getMacAddress();

  private:
    const char* ssid = "Smeraldo_Privata";
    const char* password = "smeraldoreteprivatacesenatico1";
    const char* hostname = "esp32-keyboard-bar";
};

#endif
