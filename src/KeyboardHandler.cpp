#include "KeyboardHandler.h"
#include <Arduino.h>
#include <USB.h>

void KeyboardHandler::begin() {
  Keyboard.begin();
  USB.begin();
}

void KeyboardHandler::typeText(const char* text) {
  Keyboard.press(KEY_LEFT_CTRL);
  delay(10);
  Keyboard.press('a');
  Keyboard.releaseAll();
  delay(100);
  
  while (*text) {
    Keyboard.write(*text);
    delay(10);
    text++;
  }
  Keyboard.write('\n'); //newline
}
