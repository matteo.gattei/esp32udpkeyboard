#include "WebServerHandler.h"
#include <Arduino.h>
#include <Update.h>
#include "StorageHandler.h"

WebServerHandler::WebServerHandler() : server(80) {}

void WebServerHandler::begin(StorageHandler* storageHandler) {
  this->storageHandler = storageHandler;
  
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send(200, "text/plain", "ESP32 USB HID Keyboard Server");
  });

  server.on("/latest", HTTP_GET, [this](AsyncWebServerRequest *request){
    String latestCommands = this->storageHandler->getLatestCommands();
    AsyncWebServerResponse *response = request->beginResponse(200, "text/plain", latestCommands);
    response->addHeader("X-Content-Type-Options","nosniff");
    request->send(response);
  });

  server.begin();
}

void WebServerHandler::handleClient() {
  // Not needed
}