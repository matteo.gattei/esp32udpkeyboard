#include "WiFiHandler.h"
#include <esp_wifi.h>
#include <Arduino.h>

void WiFiHandler::begin() {
  Serial.printf("Connecting to %s ", ssid);
  WiFi.begin(ssid, password);
  //WiFi.setHostname(hostname);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
    Serial.print(WiFi.status());
  }
  Serial.print("\n");
  Serial.println("!Connected!");

  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  Serial.print("MAC address: ");
  Serial.println(WiFi.macAddress());
}

String WiFiHandler::getLocalIP() {
  return WiFi.localIP().toString();
}

String WiFiHandler::getMacAddress(){
  return WiFi.macAddress();
}