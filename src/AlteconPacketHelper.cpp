#include "AlteconPacketHelper.h"

void AlteconPacketHelper::handlePacket(const char* packet, KeyboardHandler &keyboardHandler) {
    if (strlen(packet) != 16) {
        Serial.println("Invalid packet length");
        return;
    }
    Serial.println(packet);
    // Estrarre il numero della camera (CCCC) dai byte 10-13
    char roomNumber[4];  // 4 caratteri + terminatore null
    strncpy(roomNumber, packet + 11, 3);
    roomNumber[3] = '\0';  // Assicurarsi che la stringa sia terminata
    Serial.println(roomNumber);
    if (strcmp(roomNumber, "???") == 0) {
        // Se il numero della camera non è abilitato
        keyboardHandler.typeText("NON ABILITATO");
    } else {
        // Se il numero della camera è abilitato

        keyboardHandler.typeText(roomNumber);
    }
}