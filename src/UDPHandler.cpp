#include "UDPHandler.h"
#include "AlteconPacketHelper.h"
#include <Arduino.h>
#include <WiFi.h>

void UDPHandler::begin(StorageHandler *storageHandler, TimeHandler *timeHandler)
{
  this->storageHandler = storageHandler;
  this->timeHandler = timeHandler;
  Udp.begin(localUdpPort);
  Serial.printf("Now listening on UDP port %s:%d\n", WiFi.localIP().toString().c_str(), localUdpPort);
}

void UDPHandler::handlePacket(KeyboardHandler &keyboardHandler)
{
  int packetSize = Udp.parsePacket();
  if (packetSize)
  {
    Serial.printf("Packet Size: %d\n", packetSize);

    int len = Udp.read(incomingPacket, sizeof(incomingPacket) - 1);
    if (len > 0)
    {
      incomingPacket[len] = '\0';
    }

    Serial.printf("Received packet: %s\n", incomingPacket);

    AlteconPacketHelper::handlePacket(incomingPacket, keyboardHandler);

    String command = String(incomingPacket);
    String timestamp = timeHandler->getFormattedTime();
    storageHandler->saveCommand(command, timestamp);
  }
}
