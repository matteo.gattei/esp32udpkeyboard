#include "StorageHandler.h"

void StorageHandler::begin() {
  if (!SPIFFS.begin(true)) {
    Serial.println("An Error has occurred while mounting SPIFFS");
    return;
  }
}

void StorageHandler::saveCommand(const String &command, const String &timestamp) {
  File file = SPIFFS.open(filePath, FILE_APPEND);
  if (!file) {
    Serial.println("Failed to open file for appending");
    return;
  }

  file.printf("%s: %s\n", timestamp.c_str(), command.c_str());
  file.close();
  
  checkFileSize();
}

String StorageHandler::getLatestCommands() {
  File file = SPIFFS.open(filePath, FILE_READ);
  if (!file) {
    return "Failed to open file for reading";
  }

  String content;
  while (file.available()) {
    content += file.readStringUntil('\n') + "\n";
  }
  file.close();
  return content;
}

void StorageHandler::checkFileSize() {
  File file = SPIFFS.open(filePath, FILE_READ);
  if (!file) {
    Serial.println("Failed to open file for reading");
    return;
  }

  int linesCount = 0;
  while (file.available()) {
    file.readStringUntil('\n');
    linesCount++;
  }
  file.close();

  if (linesCount > 100) {
    String content = getLatestCommands();
    int pos = 0;
    for (int i = 0; i < linesCount - 100; i++) {
      pos = content.indexOf('\n', pos) + 1;
    }
    content = content.substring(pos);

    file = SPIFFS.open(filePath, FILE_WRITE);
    if (!file) {
      Serial.println("Failed to open file for writing");
      return;
    }
    file.print(content);
    file.close();
  }
}
