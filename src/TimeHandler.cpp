#include "TimeHandler.h"
#include <Arduino.h>

TimeHandler::TimeHandler() 
  : timeClient(ntpUDP, "pool.ntp.org", 0, 60000), 
    italyTZ(TimeChangeRule{"CEST", Last, Sun, Mar, 2, 120}, TimeChangeRule{"CET", Last, Sun, Oct, 3, 60}) {}

void TimeHandler::begin() {
  timeClient.begin();
  while (!timeClient.update()) {
    timeClient.forceUpdate();
  }
  updateTime();
}

String TimeHandler::getFormattedTime() {
  updateTime();
  time_t localTime = italyTZ.toLocal(timeClient.getEpochTime());
  return String(hour(localTime)) + ":" + String(minute(localTime)) + ":" + String(second(localTime)) + " " + String(day(localTime)) + "/" + String(month(localTime)) + "/" + String(year(localTime));
}

void TimeHandler::updateTime() {
  timeClient.update();
}
