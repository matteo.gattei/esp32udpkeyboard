#include "WiFiHandler.h"
#include "UDPHandler.h"
#include "WebServerHandler.h"
#include "KeyboardHandler.h"
#include "StorageHandler.h"
#include "TimeHandler.h"

WiFiHandler wifiHandler;
UDPHandler udpHandler;
WebServerHandler webServerHandler;
KeyboardHandler keyboardHandler;
StorageHandler storageHandler;
TimeHandler timeHandler;

void setup() {

  Serial.begin(9600);
  delay(1000);


  wifiHandler.begin();
  timeHandler.begin();
  storageHandler.begin();
  udpHandler.begin(&storageHandler, &timeHandler);
  webServerHandler.begin(&storageHandler);
  keyboardHandler.begin();
}

void loop() {
  udpHandler.handlePacket(keyboardHandler);
  webServerHandler.handleClient();
}
